<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestTeam extends Model
{
    protected $fillable = [
		'contest_id',
		'team_id',
	];

	public function contest()
	{
		return $this->belongsTo(Contest::class);
	}	

	public function team()
	{
		return $this->belongsTo(Team::class);
	}
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Models\User;
use Auth;

class AuthenticationController extends Controller
{
	public function __construct(){
		$this->middleware('auth:api')->except(['checkEmail', 'checkPhone', 'register', 'login']);
	}

	public function checkEmail(Request $request)
	{
		return json_encode(!User::where('email', '=', $request->email)->exists());
	}

	/**
	 * register
	 * API Registration
	 * @param  Request $request 
	 * @return response           True or false
	 */
	public function register(Request $request)
	{
		$user = new User();

		$this->validate($request, [
			'first_name' => 'required|min:3|max:20',
			'last_name' => 'nullable|min:3|max:15',
			'email' => 'required|email|unique:users'
		],
		[
			'first_name.required' => 'Please give your first name',
			'first_name.min' => 'Please give first name more than or equal 3 characters',
			'first_name.max' => 'Please give first name less than or equal 20 characters',
			'last_name.min' => 'Please give last name more than or equal 3 characters',
			'last_name.max' => 'Please give last name less than or equal 15 characters',
		]);

		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->email = $request->get('email');
		$user->password = Hash::make($request->get('password'));
		$user->api_token = bin2hex(openssl_random_pseudo_bytes(60));
		$user->save();

		if (is_null($user)) {
			return json_encode(['success' => false]);
		}
		return json_encode(['success' => true]);

		
	}	


	public function login(Request $request)
	{
		$this->validate($request, [
			'email' => 'required',
			'password' => 'required'
		],
		[
			'email.required' => 'Please give your email address',
			'password.required' => 'Please give your password'
		]);

		$user = User::where('email', $request->get('email'))->first();

		// Check if user exists in the database and if then login
		if (is_null($user)) {
			return json_encode(['success' => false, 'message' => 'No user is found by this phone no.']);
		}else{
			if (Hash::check($request->get('password'), $user->password)) {
				return json_encode(['success' => true, 'message' => 'Successfully Logged in and redirecting now...', 'user' => $user]);
			}else{
				return json_encode(['success' => false, 'message' => 'Invalid Password ! Please give valid password']);
			}
			
		}
	}
}

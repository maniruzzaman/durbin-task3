<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Models\User;
use App\Models\Contest;
use App\Models\Team;
use Auth;

class TeamsController extends Controller
{
	public function __construct(){
		$this->middleware('auth:api');
	}

	public function getPlayers(Request $request, $id)
	{	
		$user = User::where('api_token', $request->get('api_token'))->first();
		$team = Team::find($id);
		if (!is_null($team)) {
			//get the players of this team
			$players = $team->players;

			foreach ($players as $player) {
				$player->setAttribute('detail', User::find($team->user_id));
			}
			
			return json_encode(['success' => true, 'players' => $players]);
		}
	}
}

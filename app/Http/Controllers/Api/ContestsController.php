<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Models\User;
use App\Models\Contest;
use App\Models\Team;
use Auth;

class ContestsController extends Controller
{
	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(Request $request)
	{	
		$user = User::where('api_token', $request->get('api_token'))->first();
		$contests = $user->contests;
		if (!is_null($user)) {
			return json_encode(['success' => true, 'contests' => $contests]);
		}
	}

	public function store(Request $request)
	{	
		$user = User::where('api_token', $request->get('api_token'))->first();
		$contest = new Contest();
		$contest->name = $request->get('name');
		$contest->user_id = $user->id;
		$contest->save();

		if (!is_null($contest)) {
			return json_encode(['success' => true, 'contest' => $contest, 'message' => 'Contest has been created successfully !!']);
		}
	}

	public function destroy(Request $request, $id)
	{
		$user = User::where('api_token', $request->get('api_token'))->first();

		$contest = Contest::find($id);
		$contest->delete();

		return json_encode(['success' => true, 'message' => 'Contest has been deleted successfully !!']);
	}

	public function teams(Request $request, $id)
	{
		$user = User::where('api_token', $request->get('api_token'))->first();

		$contest = Contest::find($id);
		$teams = $contest->teams;

		foreach ($teams as $team) {
			$team->setAttribute('name', Team::find($team->team_id)->name);
			 // $team->attributes['name'] = Team::find($team->team_id)->name;
		}

		return json_encode(['success' => true, 'message' => 'Contest has been deleted successfully !!', 'teamsForViewed' => $teams]);
	}


	public function userTeams(Request $request)
	{
		$user = User::where('api_token', $request->get('api_token'))->first();

		$teams = $user->teams;

		if ($teams->count() > 0) {
			foreach ($teams as $team) {
				// $team->setAttribute('name', Team::find($team->team_id)->name);
			}
		}
		
		return json_encode(['success' => true, 'teams' => $teams]);
	}

	
	public function playerTeams(Request $request)
	{
		$user = User::where('api_token', $request->get('api_token'))->first();

		$teams = $user->player;

		if ($teams->count() > 0) {
			foreach ($teams as $team) {
				$team->setAttribute('name', Team::find($team->team_id)->name);
			}
		}
		
		return json_encode(['success' => true, 'teams' => $teams]);
	}

}

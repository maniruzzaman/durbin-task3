@extends('layouts.app')

@section('content')

<div class="page">
    <div class="masthead bg-primary">
        <registration-component url="{{ url('/') }}"></registration-component>
    </div>
</div>
@endsection

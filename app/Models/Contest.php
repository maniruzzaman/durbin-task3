<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contest extends Model
{
      protected $fillable = [
        'name',
        'user_id'
    ];
    
    public function owner()
    {
    	return $this->belongsTo(User::class);
    }    

    public function teams()
    {
    	return $this->hasMany(ContestTeam::class);
    }
}

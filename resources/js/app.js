
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');

 import Vuelidate from 'vuelidate';
 import Toasted from 'vue-toasted';

 Vue.use(Vuelidate);
 Vue.use(Toasted);



 Vue.component('login-component', require('./components/LoginComponent.vue'));
 Vue.component('registration-component', require('./components/RegistrationComponent.vue'));


 Vue.component('contest-component', require('./components/ContestComponent.vue'));
 Vue.component('contest-view-component', require('./components/ContestViewComponent.vue'));
 Vue.component('team-component', require('./components/TeamComponent.vue'));
 Vue.component('team-show-component', require('./components/TeamShowComponent.vue'));
 Vue.component('user-team-component', require('./components/UserTeamComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const app = new Vue({
 	el: '#app'
 });

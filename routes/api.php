<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/**
 * User Register & Login
 */
Route::post('/users/register', 'Api\AuthenticationController@register');
Route::post('/users/login', 'Api\AuthenticationController@login');

Route::get('/users/username-check', 'Api\AuthenticationController@checkUsername');
Route::get('/users/email-check', 'Api\AuthenticationController@checkEmail');
Route::get('/users/phone-check', 'Api\AuthenticationController@checkPhone');

Route::apiResource('contests', 'Api\ContestsController');
Route::get('/contest-teams/{id}', 'Api\ContestsController@teams');

Route::get('/get-teams-of-user', 'Api\ContestsController@userTeams');
Route::get('/get-teams-of-players', 'Api\ContestsController@playerTeams');

Route::get('/team/get-players/{id}', 'Api\TeamsController@getPlayers');


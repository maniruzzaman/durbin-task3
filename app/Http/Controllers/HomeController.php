<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Team;

class HomeController extends Controller
{

    public function dashboard(Request $request)
    {
        $user = User::where('api_token', $request->get('api_token'))->first();

        //Check user role
        

        if (!is_null($user)) {
            session()->flash('success', 'Yess !! Successfully Logged In.');
            return view('frontend.dashboard', compact('user'));
        }else{
            session()->flash('error', 'Sorry !! You are not authorized to access this page');
            return redirect()->route('index');
        }
    }

    public function team(Request $request, $id)
    {
    	$user = User::where('api_token', $request->get('api_token'))->first();

    	if (!is_null($user)) {
            $team = Team::find($id);
            if (!is_null($team)) {
                session()->flash('success', 'Yess !! Successfully Logged In.');
                return view('frontend.team', compact('user', 'team'));
            }else{
                session()->flash('error', 'Sorry !! You are not authorized to access this page');
                return redirect('/').'?api_token ='.$request->get('api_token');
            }

        }else{
          session()->flash('error', 'Sorry !! You are not authorized to access this page');
          return redirect()->route('index');
      }
  }
}

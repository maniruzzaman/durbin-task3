<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamPlayer extends Model
{
	protected $fillable = [
		'user_id',
		'team_id',
	];

	public function player()
	{
		return $this->belongsTo(User::class);
	}	

	public function team()
	{
		return $this->belongsTo(Team::class);
	}
}

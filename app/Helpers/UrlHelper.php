<?php 
namespace App\Helpers;

/**
 * URL Helper
 * Helps to geenerate dynamic URL based on API
 */

use App\Models\User;

class UrlHelper
{
	
	public static function generateUrlIfAuthenticated()
	{
		if (request()->get('api_token') != NULL) {
			return url('/').'?api_token='.request()->get('api_token');
		}else{
			return url('');
		}
	}
	public static function checkIfLogged()
	{
		if (request()->get('api_token') != NULL) {
			$user = User::where('api_token', request()->get('api_token'))->first();
			if (!is_null($user)) {
				return $user;
			}else{
				return null;
			}
		}
		return null;
	}
}
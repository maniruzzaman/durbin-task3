@extends('layouts.app')

@section('content')
<div class="page">
	<div class="masthead bg-primary text-center">
		<div class="container card card-body">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<h2 class="text-uppercase mb-0">Team Profile</h2>
					<hr>
					<div class="row">
						<div class="col-md-3 border-right">
							<h3>
								{{ $team->name }}
							</h3>
							<p>
								<span class="badge badge-info">{{ $team->players->count() }} Players</span>
							</p>
						</div>
						<div class="col-md-9">
							<team-show-component
							url="{{ url('/') }}"
							api_token="{{ $user->api_token }}"
							team_id={{ $team->id }}>
								
							</team-show-component>
						</div>
					</div>

				</div>
			</div>


		</div>
	</div>
</div>
@endsection

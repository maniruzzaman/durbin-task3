@extends('layouts.app')

@section('content')

<div class="page">
    <div class="masthead bg-primary text-white text-center">
      <div class="container">
        <img class="img-fluid mb-5 d-block mx-auto" src="img/profile.png" alt="">
        <h1 class="text-uppercase mb-0">Durbin Labs Task</h1>
        <hr>
        <h2>
            <a href="{{ route('login') }}" class="btn btn-outline-secondary btn-login btn-lg">Login</a>
        </h2>
        <hr>
        <div class="text-center">
        	<p class="text-danger">No account yet !!</p>
        	<h2>
        		<a href="{{ route('register') }}" class="btn btn-outline-success btn-login btn-lg">Sign Up Now</a>
        	</h2>
        </div>
    </div>
</div>
</div>


@endsection

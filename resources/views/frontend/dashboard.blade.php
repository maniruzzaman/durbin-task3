@extends('layouts.app')

@section('content')
<div class="page">
	<div class="masthead bg-primary text-center">
		<div class="container card card-body">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<h2 class="text-uppercase mb-0">Dashboard</h2>
					<h3>Welcome, {{ $user->first_name }} {{ $user->last_name }} </h3>
					<hr>
					@if ($user->contests->count() > 0)
					<contest-component url="{{ url('/') }}" api_token="{{ $user->api_token }}"> </contest-component>
					@endif

					@if ($user->teams->count() > 0)
					<team-component url="{{ url('/') }}" api_token="{{ $user->api_token }}"> </team-component>
					@endif

					@if ($user->player->count() > 0)
					<h3 class="mt-3">My Registered Teams</h3>
					<user-team-component url="{{ url('/') }}" api_token="{{ $user->api_token }}"> </user-team-component>
					@endif


				</div>
			</div>


		</div>
	</div>
</div>
@endsection

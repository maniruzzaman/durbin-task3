<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $fillable = [
		'name',
		'user_id'
	];

	public function owner()
	{
		return $this->belongsTo(User::class);
	}

	public function players()
	{
		return $this->hasMany(TeamPlayer::class);
	}
}

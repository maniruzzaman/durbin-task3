@extends('layouts.app')

@section('content')

<div class="page">
    <div class="masthead bg-primary">
        <login-component
        url="{{ url('/') }}"
        >
        </login-component>
    </div>
</div>
@endsection
